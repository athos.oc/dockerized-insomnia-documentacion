#!/bin/bash

echo "Borrando documentacion anterior"
rm -rf /app/html 
echo "Generando documentacion"
npx insomnia-documenter --config /app/insomnia.json --output /app/html 
echo "Borrando index.html de nginx"
rm -rf /var/www/html/index.nginx-debian.html
echo "Copiando documentacion a directorio nginx"
rsync -rtl /app/html/ /var/www/html/ 
echo "Levantando nginx"
nginx -g 'daemon off;'